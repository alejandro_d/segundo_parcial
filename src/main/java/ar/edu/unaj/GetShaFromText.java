package ar.edu.unaj;

import java.io.Serializable;

import java.util.*;

public class GetShaFromText implements Serializable{

	private static final long serialVersionUID = -9179889439950831790L;

	public String text;
	
	public String result;
	

	public void setText(String text){
		if (text != null)
			this.text = text;
	}
	
	
	public String getText(){
		return(this.text);	
	}
	

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}
}