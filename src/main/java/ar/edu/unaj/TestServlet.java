package ar.edu.unaj;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//mvn @WebServlet( value = "/TestServlet" )
public class TestServlet extends HttpServlet {

    private static final long   serialVersionUID    = -1L;

    public TestServlet(){
        super();
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response )
    throws ServletException, IOException {

        final String text = request.getParameter( "text" );

        HttpSession session = request.getSession(true);
        String sha = getResult(text);

        session.setAttribute("textWithSha", new TextSha(text,sha));
        response.sendRedirect( "/TextToGetSha.jsp" );
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response )
            throws ServletException, IOException {

    }

    public String getResult(String text){
        String result = "12345";
        try{
            while(!result.startsWith("AA")){
                result = Sha256.sha256(text);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

}
