package ar.edu.unaj;

import java.io.Serializable;

/**
 * Model class for Contact
 */
public class TextSha implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -576519190303713109L;

	private Integer id;
	
	private String text;
	
	private String sha;


	public TextSha(){
	}
	
	public TextSha( String text, String sha) {
		super();
		this.text = text;
		this.sha = sha;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setSha(String sha) {
		this.sha = sha;
	}
	
	public String getSha() {
		return this.sha;
	}
}
