package ar.edu.unaj;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//@WebServlet( value = "/GetShaFromTextServlet" )
public class GetShaFromTextServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(GetShaFromTextServlet.class);

    public GetShaFromTextServlet(){
        super();
    }

    private static final long   serialVersionUID    = -1L;

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response )
    throws ServletException, IOException {

        logger.info(" process post ::");
        final String text = request.getParameter( "text" );


        HttpSession session = request.getSession(true);
        String sha = getResult(text);

        logger.info(" text ::" + text);
        logger.info(" sha ::" + sha);

        request.setAttribute("textWithSha", new TextSha(text,sha));
        session.setAttribute("textWithSha", new TextSha(text,sha));
        response.sendRedirect( "/WebContent/TextToGetSha.jsp" );
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response )
            throws ServletException, IOException {

    }

    public String getResult(String text){
        String result = "12345";
        try{
            while(!(result.startsWith("A")||result.startsWith("a"))){
                result = Sha256.sha256(text);
                logger.info(" sha ::" + result);
                text += " "; // se le agregan espacios para que cambie el hash
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

}
